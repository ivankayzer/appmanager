<?php

namespace App\Http\Controllers;

use App\Services\JobOfferService;

class JobOfferController extends Controller
{
    private $jobOffers;

    public function __construct(JobOfferService $jobOffers)
    {
        $this->jobOffers = $jobOffers;
    }

    public function index()
    {
        return response()->json($this->jobOffers->get());
    }
}