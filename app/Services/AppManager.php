<?php

namespace App\Services;

use GuzzleHttp\Client as Guzzle;
use App\Transformers\TransformerAbstract;
use GuzzleHttp\Exception\BadResponseException;

class AppManager implements JobOfferService
{
    private $client;
    private $transformer;

    public function __construct(Guzzle $client, TransformerAbstract $transformer)
    {
        $this->client = $client;
        $this->transformer = $transformer;
    }

    public function get($limit = 10)
    {
        try {
            $response = $this->client->get('https://demo.appmanager.pl/api/v1/ads');
        } catch (BadResponseException  $e) {
            return abort(503, 'Failed connecting to an API. Try again later');
        }

        $response = json_decode($response->getBody(), true);

        return $this->getTransformedOutput($response, $limit);
    }

    private function getTransformedOutput($responseData, $limit)
    {
        return $this->transformer->create(array_slice($responseData['data'], 0, $limit));
    }
}