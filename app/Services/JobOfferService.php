<?php

namespace App\Services;

interface JobOfferService
{
    public function get($limit = 10);
}