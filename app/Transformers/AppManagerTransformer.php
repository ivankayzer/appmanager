<?php

namespace App\Transformers;

use Carbon\Carbon;

class AppManagerTransformer extends TransformerAbstract
{
    public function transform($offer)
    {
        return [
            'id' => $offer['id'],
            'title' => $offer['admin_name'],
            'link' => $offer['content']['url'],
            'cities' => implode(', ', $offer['cities']),
            'date_start' => Carbon::createFromFormat('Y-m-d', $offer['date_start'])->format('d F, Y')
        ];
    }
}