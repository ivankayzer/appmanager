<?php

namespace App\Transformers;

abstract class TransformerAbstract
{
    abstract public function transform($data);

    public function create($data)
    {
        return array_map(function ($item) {
            return $this->transform($item);
        }, $data);
    }
}